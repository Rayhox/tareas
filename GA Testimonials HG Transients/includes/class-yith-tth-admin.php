<?php //phpcs:ignore
/**
 * This file belongs to the YITH Testimonials HG.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_TTH_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_TTH_Admin' ) ) {
	/**
	 * YITH_TTH_Admin
	 */
	class YITH_TTH_Admin {

		/**
		 * Main Instance
		 *
		 * @var YITH_TTH_Admin
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_TTH_Admin Main instance
		 * @author Héctor García <hectorgarciam95@gmail.com>
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_TTH_Admin constructor.
		 */
		private function __construct() {

			// Crear el metabox.
			add_action( 'add_meta_boxes', array( $this, 'yith_tth_metabox' ) );

			// Guardar Datos del metabox.
			add_action( 'save_post', array( $this, 'yith_tth_metabox_save' ) );

			// Menú.
			add_action( 'admin_menu', array( $this, 'create_tth_custom_menu' ) );
			add_action( 'admin_init', array( $this, 'tth_register_settings' ) );

			// Transient.
			add_action( 'save_post', array( $this, 'tth_delete_transient_onpost' ) );
			add_action( 'delete_post', array( $this, 'tth_delete_transient_onpost' ) );

		}

		/**
		 * Setup the meta boxes
		 */
		public function yith_tth_metabox() {
			add_meta_box(
				'yith-tth-additional-information',
				__( 'Testimonial information', 'yith-testimonials-hg' ),
				array(
					$this,
					'view_meta_boxes',
				),
				YITH_TTH_Post_Types::$post_type
			);
		}

		/**
		 * View meta boxes
		 *
		 * $post
		 */
		public function view_meta_boxes( $post ) {

				$values = get_post_custom( $post->ID );
				$role = isset( $values['info_tth_role'] ) ? esc_attr( $values['info_tth_role'][0] ) : '';
				$company = isset( $values['info_tth_company'] ) ? esc_attr( $values['info_tth_company'][0] ) : '';
				$company_url = isset( $values['info_tth_company_url'] ) ? esc_attr( $values['info_tth_company_url'][0] ) : '';
				$tth_email = isset( $values['info_tth_email'] ) ? esc_attr( $values['info_tth_email'][0] ) : '';
				$rating = isset( $values['info_tth_rating'] ) ? esc_attr( $values['info_tth_rating'][0] ) : '';
				$vip = isset( $values['info_tth_vip'] ) ? esc_attr( $values['info_tth_vip'][0] ) : '';
				$badge = isset( $values['info_tth_badge'] ) ? esc_attr( $values['info_tth_badge'][0] ) : '';
				$badge_text = isset( $values['info_tth_badge_text'] ) ? esc_attr( $values['info_tth_badge_text'][0] ) : '';

				$array = array(
					'post'        => $post,
					'role'        => $role,
					'company'     => $company,
					'company_url' => $company_url,
					'tth_email'   => $tth_email,
					'rating'      => $rating,
					'vip'         => $vip,
					'badge'       => $badge,
					'badge_text'  => $badge_text,
				);
				yith_tth_get_view( '/metaboxes/yith_tth_metabox.php', $array );

		}

		/** //phpcs:ignore
		 * Save metabox values
		 * $post_id
		 */
		public function yith_tth_metabox_save( $post_id ) {
			if ( ! current_user_can( 'edit_post' ) ) {
				return;
			}

			if ( YITH_TTH_Post_Types::$post_type !== get_post_type( $post_id ) ) {
				return;
			}

			if ( isset( $_POST['info_tth_role'] ) ) { //phpcs:ignore
				update_post_meta( $post_id, 'info_tth_role', wp_kses( $_POST['info_tth_role'], $allowed ) );//phpcs:ignore

			}

			if ( isset( $_POST['info_tth_company'] ) ) {//phpcs:ignore
				update_post_meta( $post_id, 'info_tth_company', esc_attr( $_POST['info_tth_company'] ) );//phpcs:ignore
			}

			if ( isset( $_POST['info_tth_company_url'] ) ) {//phpcs:ignore
				update_post_meta( $post_id, 'info_tth_company_url', esc_attr( $_POST['info_tth_company_url'] ) );//phpcs:ignore
			}

			if ( isset( $_POST['info_tth_email'] ) ) {//phpcs:ignore
				update_post_meta( $post_id, 'info_tth_email', esc_attr( $_POST['info_tth_email'] ) );//phpcs:ignore
			}

			if ( isset( $_POST['info_tth_rating'] ) ) {//phpcs:ignore
				update_post_meta( $post_id, 'info_tth_rating', esc_attr( $_POST['info_tth_rating'] ) );//phpcs:ignore
			}

			if ( isset( $_POST['info_tth_vip'] ) ) {//phpcs:ignore
				update_post_meta( $post_id, 'info_tth_vip', esc_attr( $_POST['info_tth_vip'] ) );//phpcs:ignore
			}

			if ( isset( $_POST['info_tth_badge'] ) ) {//phpcs:ignore
				update_post_meta( $post_id, 'info_tth_badge', esc_attr( $_POST['info_tth_badge'] ) );//phpcs:ignore
			}

			if ( isset( $_POST['info_tth_badge_text'] ) ) {//phpcs:ignore
				update_post_meta( $post_id, 'info_tth_badge_text', esc_attr( $_POST['info_tth_badge_text'] ) );//phpcs:ignore
			}

		}
		/**
		 * Creates the custom menú to display on backend.
		 *
		 * @return void
		 */
		public function create_tth_custom_menu() {//phpcs:ignore

			add_menu_page(
				esc_html__( 'TTH Custom Options', 'yith-testimonials-hg' ),
				esc_html__( 'TTH Custom Options', 'yith-testimonials-hg' ),
				'manage_options',
				'tth_plugin_options',
				array( $this, 'tth_custom_menu_page' ),
				'',
				40
			);

		}
		/**
		 *  Llamada a la vista de las  opciones custom.
		 */
		public function tth_custom_menu_page() {//phpcs:ignore

			yith_tth_get_view( '/admin/testimonials-options-panel.php', array() );
		}

		/**
		 * Add the fields in the shortcode attribute management page.
		 */
		public function tth_register_settings() {//phpcs:ignore

			$page_name    = 'tth-options-page';
			$section_name = 'options_section';

			$setting_fields = array(
				array(
					'id'       => 'yith_tth_shortcode_number',
					'title'    => esc_html__( 'Ids to display', 'yith-testimonials-hg' ),
					'callback' => 'print_ids_input',
				),
				array(
					'id'       => 'yith_tth_shortcode_show_image',
					'title'    => esc_html__( 'Display img', 'yith-testimonials-hg' ),
					'callback' => 'print_show_image',
				),
				array(
					'id'       => 'yith_tth_shortcode_effect',
					'title'    => esc_html__( 'Effect', 'yith-testimonials-hg' ),
					'callback' => 'print_effect',
				),
				array(
					'id'       => 'yith_tth_shortcode_borderradius',
					'title'    => esc_html__( 'Border radius', 'yith-testimonials-hg' ),
					'callback' => 'tth_print_borderradius',
				),
				array(
					'id'       => 'yith_tth_shortcode_color',
					'title'    => esc_html__( 'Link color', 'yith-testimonials-hg' ),
					'callback' => 'tth_print_color',
				),
			);

			add_settings_section(
				$section_name,
				esc_html__( '', 'yith-testimonials-hg' ),//phpcs:ignore
				'',
				$page_name
			);

			foreach ( $setting_fields as $field ) {
				extract( $field );//phpcs:ignore

				add_settings_field(
					$id,
					$title,
					array( $this, $callback ),
					$page_name,
					$section_name,
					array( 'label_for' => $id ),
				);

				register_setting( $page_name, $id );
			}

		}

		/**
		 * Ids option for custom options menu.
		 */
		public function print_ids_input() {//phpcs:ignore
			$tth_number = intval( get_option( 'yith_tth_shortcode_number', 6 ) );
			?>
			<label> <?php _e( 'Ids to display on the page' ); ?> </label>
			<input type="number" id="yith_tth_shortcode_number" name="yith_tth_shortcode_number"
				value="<?php echo '' !== $tth_number ? $tth_number : 6; ?>">
			<?php
		}

		/**
		 * Img display option for custom options menu.
		 */
		public function print_show_image() {//phpcs:ignore
			?>
			<hr>
			<br>
			<input type="checkbox" checked class="tth-option-panel__onoff__input" name="yith_tth_shortcode_show_image"
				value='yes'
				id="yith_tth_shortcode_show_image"
				<?php checked( get_option( 'yith_tth_shortcode_show_image', '' ), 'yes' ); ?>
			>
			<label> <?php _e( 'Check if you want the front img displayed on the page' ); ?> </label>

			<label for="shortcode_show_image" class="tth-option-panel__onoff__label ">
				<span class="tth-option-panel__onoff__btn"></span>
			</label>
			<?php
		}

		/**
		 * Custom effects option for custom options menu.
		 *
		 * @return void
		 */
		public function print_effect() {//phpcs:ignore
			?>
			<hr>
			<br>
			<select name="yith_tth_shortcode_effect" id= 'yith_tth_shortcode_effect'>  
				<option <?php checked( get_option( 'yith_tth_shortcode_effect', '' ), 'yes' ); ?>>none</option>  
				<option <?php checked( get_option( 'yith_tth_shortcode_effect', '' ), 'yes' ); ?>>zoom</option>  
				<option <?php checked( get_option( 'yith_tth_shortcode_effect', '' ), 'yes' ); ?>>highlight</option> 
			</select>
			<label> <?php _e( 'Choose the effect you want to display on the page' ); ?> </label>
			<?php
		}

		/**
		 * Border radius option for custom options menu.
		 *
		 * @return void
		 */
		public function tth_print_borderradius() {//phpcs:ignore
			$border_radius = intval( get_option( 'yith_tth_shortcode_borderradius', 7 ));
			?>
			<input type="number" id="yith_tth_shortcode_borderradius" name="yith_tth_shortcode_borderradius"
				value="<?php echo '' !== $border_radius ? $border_radius : 7; ?>">
			<?php
		}

		/**
		 * Color optiom for custom options menu.
		 *
		 * @return void
		 */
		public function tth_print_color() {//phpcs:ignore
			$color = get_option( 'yith_tth_shortcode_color', '#0073aa' );
			?>
			<input type="color" name="yith_tth_shortcode_color" id="yith_tth_shortcode_color" value="<?php echo esc_attr( $color ); ?>">
			<?php
		}

		/**
		 * Delete the current transient on a testimonial save or delete
		 *
		 * @return void
		 */
		public function tth_delete_transient_onpost() {//phpcs:ignore

			global $post;

			if ( $post && $post->post_type ) {

				if ( 'yith_hg_testimonial' === $post->post_type) {

					if ( $transient ) {
						delete_transient( 'yith_tth_custom_transient' );
						// error_log( print_r( 'Entro al guardar o borrar',true )  );
					}
				}
			}
		}
	}
}
