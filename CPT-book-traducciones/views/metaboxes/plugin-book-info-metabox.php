<?php

?>

<!-- Testimonial isbn field -->
<div>
    <p>
		<label>
		<?php _e('ISBN','yith-plugin-book');?>
		</label>
		<input type="text" name="info_book_isbn" value="<?php echo esc_html( $isbn ); ?>" />
	</p>
	<p>
		<label>
		<?php _e('Price','yith-plugin-book');?>
		</label>
		<input type="number"  name="info_book_price" value="<?php echo esc_html( $price ); ?>" />
	</p> 
	<p>  
		<label>
		<?php _e('Cover type','yith-plugin-book');?>
		</label>
        <select name="info_book_cover">  
            <option <?php selected( $cover, 'dura' ); ?>>
			
			<?php _e('Tough','yith-plugin-book');?>
			
			</option>  
            <option <?php selected( $cover, 'blanda' ); ?>>
			
			<?php _e('Soft','yith-plugin-book');?>
			
			</option>  
        </select>  
	</p>
	<p>
		<label>
		<?php _e('Idiom','yith-plugin-book');?>
		</label>
		<input type="text" name="info_book_idiom" value="<?php echo esc_html( $idiom ); ?>" />
	</p>
</div>

