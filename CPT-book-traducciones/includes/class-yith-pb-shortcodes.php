<?php
/*
 * This file belongs to the YITH CPT Book.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_PB_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PB_Shortcodes' ) ) {

	class YITH_PB_Shortcodes {

        /**
		 * Main Instance
		 *
		 * @var YITH_PB_Shortcodes
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		
        /**
         * Main plugin Instance
         *
         * @return YITH_PB_Post_Types Main instance
         * @author Héctor García <hectorgarciam95@gmail.com>
         */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
        }
        
		/**
		 * YITH_PS_Post_Types constructor.
		 */
		private function __construct() {
			
			
			$shortcodes = array(
				'yith_pb_show_post_type' => __CLASS__ . '::yith_displaycustompost_function', // print books.
				
			);
			foreach ( $shortcodes as $shortcode => $function ) {
				add_shortcode( $shortcode, $function );
			}
			
		}


		//Display books and their content
		public static function yith_displaycustompost_function($atts){
	
			error_log(print_r($atts, true));
			
			
			$args = array(
							'post_type'      => 'book',
							'posts_per_page' => get_option('yith_cpt_shortcode_number', 5),
							'publish_status' => 'published',
						 );
		 
			$query = new WP_Query($args);
			
			ob_start();
		 
			if($query->have_posts()) :
		 
				while($query->have_posts()) :
		 
					$query->the_post() ;
						if(get_option('yith_cpt_shortcode_show_image')){
							echo '<hr>';
							_e('<p><strong> Title: </strong> <h1>' . get_the_title() . '</h1></p>', 'yith-plugin-book' );
							_e('<p><strong> Desciption: </strong>' . get_the_content() . '</p>', 'yith-plugin-book' );
							_e( '<p><strong> Cover image: </strong>'  . get_the_post_thumbnail() . '</p>', 'yith-plugin-book');
							_e( '<p><strong> ISBN: </strong>' . get_post_meta( get_the_ID(), 'info_book_isbn', true ) . '</p>', 'yith-plugin-book');
							_e( '<p><strong> Price: </strong>' . get_post_meta( get_the_ID(), 'info_book_price', true ) . ' €</p>', 'yith-plugin-book');
							_e( '<p><strong> Cover type: </strong>' . get_post_meta( get_the_ID(), 'info_book_cover', true ) . '</p>', 'yith-plugin-book');
							_e( '<p><strong> Idiom: </strong>' . get_post_meta( get_the_ID(), 'info_book_idiom', true ) . '</p>', 'yith-plugin-book');
							echo '<hr>';
						
						}else if(! get_option('yith_cpt_shortcode_show_image')){

							echo'<hr>';
							_e('<p><strong> Title: </strong> <h1>' . get_the_title() . '</h1></p>', 'yith-plugin-book' );
							_e('<p><strong> Desciption: </strong>' . get_the_content() . '</p>', 'yith-plugin-book' );
							_e('<p><strong> ISBN: </strong>' . get_post_meta( get_the_ID(), 'info_book_isbn', true ) . '</p>', 'yith-plugin-book' );
							_e('<p><strong> Price: </strong>' . get_post_meta( get_the_ID(), 'info_book_price', true ) . ' €</p>', 'yith-plugin-book' );
							_e('<p><strong> Cover type: </strong>' . get_post_meta( get_the_ID(), 'info_book_cover', true ) . '</p>', 'yith-plugin-book' );
							_e('<p><strong> Idiom: </strong>' . get_post_meta( get_the_ID(), 'info_book_idiom', true ) . '</p>', 'yith-plugin-book' );
							echo '<hr>';
						}

					
				endwhile;
			
		 
				wp_reset_postdata();
		 
			endif;
			
			$value = ob_get_clean();
			
			return $value;
			
			//error_log(print_r($query,true));
		}



	}	
}