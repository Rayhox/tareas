<?php
/**
 * Plugin Name:       yith_enqueue_scripts_and_styles
 * Plugin URI:        https://example.com/plugins/the-basics/
 * Description:       Ejercicio de enqueue
 * Version:           1.0.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Héctor García
 * Author URI:        https://yithemes.com/
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       yith-enqueue-script
 * Domain Path:       /languages
*/

global $post; 

function adding_styles(){

  //echo(plugin_dir_url(__FILE__) . 'assets/css/yith_tarea_hg.css');
  
  wp_register_style( 'enqueue_scripts', //Nombre del plugin
  
  plugin_dir_url(__FILE__) . 'assets/css/yith_tarea_hg.css' ); //directorio del css

  wp_enqueue_style('enqueue_scripts'); //Llamada al encolado
  
}
   //var_dump( is_single());
       

    

// //Prueba de fallo en las query
//    function footer_css() {
//     if (is_page() || is_product() || is_singular('post') || is_woocommerce() || is_cart() || is_checkout()) {
//         wp_enqueue_style('Footer_Links-style', get_stylesheet_directory_uri() . '/CSS_Components/Footer_Links.css?v=2.5', array( 'parent-style'));
//     }
// }


// add_action( 'template_redirect', 'footer_css' );

    
// Le he preguntado a Fran y me dijo que la forma de hacerlo es con el "is_single()", pero me salta un error todo el rato, para lo cual él me pasó una posible solución
// (El código comentado arriba)
// Pero tampoco me ha funciona, la función "adding_styles" estoy seguro que funciona porque lo he comprobado, pero el is_single siempre me da error 
// (is_singular was called <strong>incorrectly</strong>. Conditional query tags do not work before the query is run. Before then, they always return false.)

/*if( is_single('post')  ){
            add_action('wp_enqueue_scripts' , 'adding_styles');
      }
 */

 
 
 
 
 
 
 ?>