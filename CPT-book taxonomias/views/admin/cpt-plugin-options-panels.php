<?php 

?>

<div class="wrap">
    <h1><?php esc_html_e('Book Options', 'yith-plugin-book' );?></h1>

    <form method="post" action="options.php">
        <?php
            settings_fields( 'cpt-options-page' );
		    do_settings_sections( 'cpt-options-page' );
           esc_html__( submit_button('Modify') );
        ?>

    </form>
</div>