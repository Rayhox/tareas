<?php

?>

<!-- Testimonial isbn field -->
<div>
    <p>
		<label>ISBN</label>
		<input type="text" name="info_book_isbn" value="<?php echo esc_html( $isbn ); ?>" />
	</p>
	<p>
		<label>Price</label>
		<input type="number"  name="info_book_price" value="<?php echo esc_html( $price ); ?>" />
	</p> 
	<p>  
		<label>Cover type</label>  
        <select name="info_book_cover">  
            <option <?php selected( $cover, 'dura' ); ?>>Dura</option>  
            <option <?php selected( $cover, 'blanda' ); ?>>Blanda</option>  
        </select>  
	</p>
	<p>
		<label>Idiom</label>
		<input type="text" name="info_book_idiom" value="<?php echo esc_html( $idiom ); ?>" />
	</p>
</div>

