<?php
/*
 * This file belongs to the YITH CPT Book.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_PB_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PB_Admin' ) ) {

	class YITH_PB_Admin {

        /**
		 * Main Instance
		 *
		 * @var YITH_PB_Admin
		 * @since 1.0
		 * @access private
		 */

		private static $instance;
		/**
		 * Main plugin Instance
         * @return YITH_PB_Admin Main instance
         * @author Héctor García <hectorgarciam95@gmail.com>
         */
		
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
        }

		/**
		 * YITH_PB_Admin constructor.
		 */
		private function __construct() {
			
			// Crear el metabox.
			add_action( 'add_meta_boxes', array( $this, 'yith_book_metabox' ) );
			
			//Guardar Datos del metabox
			add_action( 'save_post', array( $this, 'yith_book_metabox_save') );
			
			// Creación de las columnas custom.
			add_filter('manage_book_posts_columns', array( $this, 'add_custom_colums' ) );

			// Contenido de las columnas.
			add_action('manage_book_posts_custom_column', array( $this, 'display_custom_colums'), 10, 2 );

			// Menú.
			add_action('admin_menu', array( $this, 'create_cpt_custom_menu') );
			add_action( 'admin_init', array( $this, 'cpt_register_settings' ) );
		}

		/**
		 * Setup the meta boxes
		 */
		public function yith_book_metabox() {
			add_meta_box( 'yith-ps-additional-information', __( 'Additional information', 'yith-plugin-pb' ), array(
			$this, 'view_meta_boxes' ), YITH_PB_Post_Types::$post_type );
		}

		/**
		 * View meta boxes
		 *
		 * @param $post
		 */
		public function view_meta_boxes( $post ) {
			
				$values = get_post_custom( $post->ID );
				$isbn = isset( $values['info_book_isbn'] ) ? esc_attr( $values['info_book_isbn'][0] ) : '';
				$price = isset( $values['info_book_price'] ) ? esc_attr( $values['info_book_price'][0] ) : '';
				$cover = isset( $values['info_book_cover'] ) ? esc_attr( $values['info_book_cover'][0] ) : '';
				$idiom = isset( $values['info_book_idiom'] ) ? esc_attr( $values['info_book_idiom'][0] ) : '';
			
				$array = array(
					'post'  => $post,
					'isbn'  => $isbn,
					'price' => $price,
					'cover' => $cover,
					'idiom' => $idiom,
				);
			yith_pb_get_view( '/metaboxes/plugin-book-info-metabox.php', $array);
		}

		/**
		 * Save metabox values
		 * @param $post_id
		 */
		public function yith_book_metabox_save( $post_id ) {
			// validate if user can edit the post
			if ( ! current_user_can( 'edit_post' ) ) {
				return;
			}

			if ( YITH_PB_Post_Types::$post_type !== get_post_type( $post_id ) ) {
				return;
			}

			if ( isset( $_POST['info_book_isbn'] ) ) {
				update_post_meta( $post_id, 'info_book_isbn', wp_kses( $_POST['info_book_isbn'], $allowed ) );
				
			}
		
			if ( isset( $_POST['info_book_price'] ) ) {
				update_post_meta( $post_id, 'info_book_price', esc_attr( $_POST['info_book_price'] ) );
			}
		
			if ( isset( $_POST['info_book_cover'] ) ) {
				update_post_meta( $post_id, 'info_book_cover', esc_attr( $_POST['info_book_cover'] ) );
			}
		
			if ( isset( $_POST['info_book_idiom'] ) ) {
				update_post_meta( $post_id, 'info_book_idiom', esc_attr( $_POST['info_book_idiom'] ) );
			}
		}
		
		// Creación de las columnas.
		public function add_custom_colums( $post_columns ) {

			$new_columns = apply_filters('yith_book_custom_columns ', array(
				'isbn'         => esc_html__('ISBN','yith-CPT-book'),
				'price'        => esc_html__('Price','yith-CPT-book'),
				'covertype'   => esc_html__('Cover type' , 'yith-CPT-book'),
				'idiom'        => esc_html__('Idiom' , 'yith-CPT-book'),
			) );
			
			unset  ( $post_columns ['date'] );

			$post_columns = array_merge( $post_columns, $new_columns );

			return $post_columns;
			
			//error_log(print_r($post_columns,true));
		}
		
		// Contenido de las columnas.
		public function display_custom_colums( $column_name, $post_id ) {

			switch ( $column_name ) {

				case 'isbn' :
						echo esc_html( get_post_meta( get_the_ID(), 'info_book_isbn', true) );	
					break;
				
					case 'price' :
						echo esc_html( get_post_meta( get_the_ID(), 'info_book_price', true) );
					break;

					case 'covertype' :
						echo esc_html( get_post_meta( get_the_ID(), 'info_book_cover', true) );
					break;

					case 'idiom' :
						echo esc_html( get_post_meta( get_the_ID(), 'info_book_idiom', true) );
						break;

				default  : do_action('yith_ps_skeleton_display_custom_column',$column,$post_id);
					break;
			}

		}
		
		// Creando el menú
		public function create_cpt_custom_menu() {

			
				add_menu_page( 
					esc_html__( 'CPT Custom Options', 'yith-plugin-book' ),
					esc_html__( 'CPT Custom Options', 'yith-plugin-book' ),
					'manage_options',
					'cpt_plugin_options',
					array($this,'cpt_custom_menu_page'),
					'',
					40
				); 

		}
		/**
		 *  Llamada a la vista de las  opciones custom
		 */
		function cpt_custom_menu_page() {
			
			//error_log(print_r('HOLA SOY CONCHA, ENTRO' , true));
			yith_pb_get_view('/admin/cpt-plugin-options-panels.php', array() );
		}

		/**
		 * Add the fields in the shortcode attribute management page
		 */

		public function cpt_register_settings() {
			
			$page_name    = 'cpt-options-page';
			$section_name = 'options_section';

			$setting_fields = array(
				array(
					'id'       => 'yith_cpt_shortcode_number',
					'title'    => esc_html__( 'Books per page', 'yith-plugin-book' ),
					'callback' => 'print_number_input'
				),
				array(
					'id'       => 'yith_cpt_shortcode_show_image',
					'title'    => esc_html__( 'Display img', 'yith-plugin-book' ),
					'callback' => 'print_show_image'
				),
			);

			add_settings_section(
				$section_name,
				esc_html__( '', 'yith-plugin-book' ),
				'',
				$page_name
			);

			foreach ( $setting_fields as $field ) {
				extract( $field );

				add_settings_field(
					$id,
					$title,
					array( $this, $callback ),
					$page_name,
					$section_name,
					array( 'label_for' => $id )
				);

				register_setting( $page_name, $id );
			}


		}

		/**
		 * Opción de post_per_page.
		 */
		public
		function print_number_input() {
			$tst_number = intval( get_option( 'yith_cpt_shortcode_number', 5 ) );
			?>
            <input type="number" id="yith_cpt_shortcode_number" name="yith_cpt_shortcode_number"
                   value="<?php echo '' !== $tst_number ? $tst_number : 6; ?>">
			<?php
		}

		/**
		 * Opción del display img.
		 */
		public
		function print_show_image() {
			?>
			<hr>
			<br>
            <input type="checkbox" checked class="cpt-tst-option-panel__onoff__input" name="yith_cpt_shortcode_show_image"
                   value='yes'
                   id="yith_cpt_shortcode_show_image"
				<?php checked( get_option( 'yith_cpt_shortcode_show_image', '' ), 'yes' ); ?>
				
            >
			<label>Check if you want the front img displayed on the page</label>
            
			<label for="shortcode_show_image" class="cpt-tst-option-panel__onoff__label ">
                <span class="cpt-tst-option-panel__onoff__btn"></span>
            </label>
			<?php
		}
		
	}	
}