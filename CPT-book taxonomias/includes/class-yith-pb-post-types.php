<?php
/*
 * This file belongs to the YITH CPT Book.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_PB_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PB_Post_Types' ) ) {

	class YITH_PB_Post_Types {

        /**
		 * Main Instance
		 *
		 * @var YITH_PB_Post_Types
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		/**
		 * Post type name
		 *
		 * @var YITH_PB_Post_Types
		 * @since 1.0
		 * @access public
		 */
		public static $post_type = 'book';
		
		/**
         * Main plugin Instance
         *
         * @return YITH_PB_Post_Types Main instance
         * @author Héctor García <hectorgarciam95@gmail.com>
         */
		
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
        }
        
		/**
		 * YITH_PB_Post_Types constructor.
		 */
		private function __construct() {
			add_action( 'init', array( $this, 'yith_create_post_type_book' ) );
			add_action('init',array( $this ,'cpt_register_taxonomy'));

		}

		/**
		 * Setup the 'Book' custom post type
		 */
		function yith_create_post_type_book() {
			$labels = array(
				'name'        => _x( 'Book', 'Post type general name', 'yith-plugin-book' ),
				'description' => _x( 'Book', 'Post type description', 'yith-plugin-book' ),
				'img'         => _x( 'Book', 'Post type img', 'yith-plugin-book' ),
			);
	   
			   $args = array(
				   'labels'             => $labels,
				   'public'             => true,
				   'publicly_queryable' => true,
				   'show_ui'            => true,
				   'show_in_menu'       => true,
				   'query_var'          => true,
				   'rewrite'            => array( 'slug' => 'book' ),
				   'capability_type'    => 'post',
				   'has_archive'        => true,
				   'hierarchical'       => false,
				   'menu_position'      => null,
				   'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),
			   );
			   register_post_type( self::$post_type, $args );
	   }

	   public function cpt_register_taxonomy() {
		// Add new taxonomy, make it hierarchical (like categories)
		$labels = array(
		  'name'              => _x( 'Autor Taxonomy', 'taxonomy general name', 'yith-plugin-book' ),
		  'singular_name'     => _x( 'Autor', 'taxonomy singular name', 'yith-plugin-book' ),
		  'search_items'      => __( 'Search Autor', 'yith-plugin-book' ),
		  'all_items'         => __( 'All Autores', 'yith-plugin-book' ),
		  'parent_item'       => __( 'Autor Parent', 'yith-plugin-book' ),
		  'parent_item_colon' => __( 'Autor Parent :', 'yith-plugin-book' ),
		  'edit_item'         => __( 'Edit Autor', 'yith-plugin-book' ),
		  'update_item'       => __( 'Update Autor', 'yith-plugin-book' ),
		  'add_new_item'      => __( 'Add New Autor', 'yith-plugin-book' ),
		  'new_item_name'     => __( 'New Autor Name', 'yith-plugin-book' ),
		  'menu_name'         => __( 'Autor', 'yith-plugin-book' ),
	  );
   
	  $args = array(
		  'hierarchical'      => false,
		  'labels'            => $labels,
		  'show_ui'           => true,
		  'show_admin_column' => true,
		  'query_var'         => true,
		  'rewrite'           => array( 'slug' => 'yith_tax_autor' ),
	  );
   
	  register_taxonomy( 'yith_cpt_autor_tax', array( self::$post_type ), $args );

	 // Add new taxonomy, make it no hierarchical (like tags)

	  $labels1 = array(
		  'name'              => _x( 'Editorial taxonomy', 'taxonomy general name', 'yith-plugin-book' ),
		  'singular_name'     => _x( 'Editorial', 'taxonomy singular name', 'yith-plugin-book' ),
		  'search_items'      => __( 'Search Editorial', 'yith-plugin-book' ),
		  'all_items'         => __( 'All Editoriales', 'yith-plugin-book' ),
		  'parent_item'       => __( 'Editorial Parent', 'yith-plugin-book' ),
		  'parent_item_colon' => __( 'Editorial Parent:', 'yith-plugin-book' ),
		  'edit_item'         => __( 'Edit Editorial', 'yith-plugin-book' ),
		  'update_item'       => __( 'Update Editorial', 'yith-plugin-book' ),
		  'add_new_item'      => __( 'Add New Editorial', 'yith-plugin-book' ),
		  'new_item_name'     => __( 'New Editorial Name', 'yith-plugin-book' ),
		  'menu_name'         => __( 'Editorial', 'yith-plugin-book' ),
	  );

	  $args1 = array(
		  'hierarchical'      => false,
		  'labels'            => $labels1,
		  'show_ui'           => true,
		  'show_admin_column' => true,
		  'query_var'         => true,
		  'rewrite'           => array( 'slug' => 'yith_tax_editorial' ),
	  );

	  register_taxonomy( 'yith_cpt_editorial_tax', array( self::$post_type ), $args1 );

  }
	}
}