<?php //phpcs:ignore
/**
 * This file belongs to the YITH Datatables HG.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_DTH_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_DTH_Shortcodes' ) ) {
	/**
	 * YITH_DTH_Shortcodes
	 */
	class YITH_DTH_Shortcodes {

		/**
		 * Main Instance
		 *
		 * @var YITH_DTH_Shortcodes
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_DTH_Post_Types Main instance
		 * @author Héctor García <hectorgarciam95@gmail.com>
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}
		/**
		 * YITH_DTH_Shortcode constructor.
		 */
		private function __construct() {

			$shortcodes = array(
				'yith_datatables' => __CLASS__ . '::yith_display_datatable_form',
			);
			foreach ( $shortcodes as $shortcode => $function ) {
				add_shortcode( $shortcode, $function );
			}
		}
		/**
		 * Custom shortcode to display datatable form.
		 *
		 * $atts
		 * @return void
		 */
		public static function yith_display_datatable_form() {

			ob_start();

			yith_dth_get_template( '/frontend/show_raffle.php' );

			return ob_get_clean();
		}
	}
}
