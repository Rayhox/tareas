<?php
/**
 * This file belongs to the YITH Datatables HG.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'YITH_DTH_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_DTH_Datatables_Hg' ) ) {
	/**
	 * YITH_DTH_Datatables_Hg
	 */
	class YITH_DTH_Datatables_Hg {

		/**
		 * Main Instance
		 *
		 * @var YITH_DTH_Datatables_Hg
		 * @since 1.0
		 * @access private
		 */

		private static $instance;
		/**
		 * Main Admin Instance
		 *
		 * @var YITH_DTH_Datatables_Hg_Admin
		 * @since 1.0
		 */
		public $admin = null;
		/**
		 * Main Frontpage Instance
		 *
		 * @var YITH_DTH_Datatables_Hg_Frontend
		 * @since 1.0
		 */
		public $frontend = null;

		/**
		 * Main Shortcodes Instance
		 *
		 * @var YITH_DTH_Datatables_Hg_Frontend
		 * @since 1.0
		 */
		public $shortcodes = null;

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_DTH_Datatables_Hg Main instance
		 * @author Héctor García <hectorgarciam95@gmail.com>
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();

		}
		/**
		 * YITH_DTH_Datatables_Hg constructor.
		 */
		private function __construct() {
			$require = apply_filters(
				'yith_dth_require_class',
				array(
					'common'   => array(
						'includes/functions.php',
						'includes/class-yith-dth-shortcodes.php',
						'includes/class-yith-dth-ajax.php',
						'includes/class-yith-dth-wpdb.php',

					),
					'admin'    => array(
						'includes/class-yith-dth-admin.php',
					),
					'frontend' => array(
						'includes/class-yith-dth-frontend.php',
					),
				)
			);

			$this->_require( $require );

			$this->init_classes();

			$this->init();
		}
		/**
		 * Add the main classes file
		 *
		 * Include the admin and frontend classes
		 *
		 * $main_classes array The require classes file path
		 *
		 * @author Héctor García <hectorgarciam95@gmail.com>
		 * @since  1.0
		 *
		 * @return void
		 * @access protected
		 */
		protected function _require( $main_classes ) {
			foreach ( $main_classes as $section => $classes ) {
				foreach ( $classes as $class ) {
					if ( 'common' == $section || ( 'frontend' == $section && !is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) || ( 'admin' == $section && is_admin() ) && file_exists( YITH_DTH_DIR_PATH . $class ) ) {
						require_once( YITH_DTH_DIR_PATH . $class );
					}
				}
			}
		}

		/**
		 * Init common class if they are necessary
		 *
		 * @author Héctor García <hectorgarciam95@gmail.com>
		 *
		 * @since  1.0
		 **/
		public function init_classes() {
			$this->shortcodes = YITH_DTH_Shortcodes::get_instance();
			YITH_DTH_AJAX::get_instance();
		}

		/**
		 * Function init()
		 *
		 * Instance the admin or frontend classes
		 *
		 * @author Héctor García <hectorgarciam95@gmail.com>
		 * @since  1.0
		 * @return void
		 * @access protected
		 */
		public function init() {
			if ( is_admin() ) {
				$this->admin = YITH_DTH_Admin::get_instance();
			}

			if ( ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
				$this->frontend = YITH_DTH_Frontend::get_instance();
			}
		}

	}
}
/**
 * Get the YITH_DTH_Datatables_Hg instance
 *
 * @return YITH_DTH_Datatables_Hg
 */
if ( ! function_exists( 'yith_dth_datatables_hg' ) ) {
	/**
	 * Yith_dth_datatables_hg
	 *
	 * @return void
	 */
	function yith_dth_datatables_hg() {
		return YITH_DTH_Datatables_Hg::get_instance();
	}
}
