<?php //phpcs:ignore
/**
 * This file belongs to the YITH Datatables HG.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'YITH_DTH_DB' ) ) {
	/**
	 * YITH WooCommerce Auction Database
	 *
	 * @since 1.0.0
	 */
	class YITH_DTH_DB {

		/**
		 * DB version
		 *
		 * @var string
		 */
		public static $version = '1.0.0';

		public static $auction_table = 'yith_raffle';


		/**
		 * Constructor
		 *
		 * @return YITH_DTH_DB
		 */
		private function __construct() {
		}

		/**
		 * Install the DB.
		 *
		 * @return void
		 */
		public static function install() {
			self::create_db_table();
		}

		/**
		 * Create table for Notes
		 *
		 * @param bool $force
		 */
		public static function create_db_table( $force = false ) {
			global $wpdb;

			$current_version = get_option( 'yith_dth_db_version' );

			if ( $force || $current_version != self::$version ) {
				$wpdb->hide_errors();

				$table_name      = $wpdb->prefix . self::$auction_table;
				$charset_collate = $wpdb->get_charset_collate();

				$sql
					= "CREATE TABLE $table_name (
					`id` bigint(20) NOT NULL AUTO_INCREMENT,
					`user_id` bigint(20) NOT NULL,
					`name` varchar(255) NOT NULL,
					`surname` varchar(255) NOT NULL,
					`email` varchar(255) NOT NULL,
					PRIMARY KEY (id)
					) $charset_collate;";

				if ( ! function_exists( 'dbDelta' ) ) {
					require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
				}
				dbDelta( $sql );
				update_option( 'yith_dth_db_version', self::$version );
			}
		}

	}
}
