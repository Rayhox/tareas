<?php //phpcs:ignore
/**
 * This file belongs to the YITH Datatables HG.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_DTH_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_DTH_Admin' ) ) {
	/**
	 * YITH_DTH_Admin
	 */
	class YITH_DTH_Admin {

		/**
		 * Main Instance
		 *
		 * @var YITH_DTH_Admin
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_DTH_Admin Main instance
		 * @author Héctor García <hectorgarciam95@gmail.com>
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_DTH_Admin constructor.
		 */
		private function __construct() {

		}

	}
}
