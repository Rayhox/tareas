<?php //phpcs:ignore
/**
 * This file belongs to the YITH Datatables HG.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_DTH_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_DTH_AJAX' ) ) {

	/**
	 * YITH_DTH_AJAX
	 */
	class YITH_DTH_AJAX {

		/**
		 * Main Instance
		 *
		 * @var YITH_DTH_AJAX
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_DTH_AJAX Main instance
		 * @author Héctor García <hectorgarciam95@gmail.com>
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_DTH_AJAX constructor.
		 */
		private function __construct() {
			add_action( 'wp_ajax_take_raffle', array( $this, 'check_form' ) );
			add_action( 'wp_ajax_nopriv_take_raffle', array( $this, 'check_form' ) );
		}
		/**
		 * Updating likes through AJAX
		 *
		 * @return void
		 */
		public function check_form() {

			$valid_data = true;
			$user_data  = $_POST['raffle_data'];
			$mensaje    = '';

			if ( 'no' === $_POST['require'] ) {
				$mensaje = esc_html__( 'You must check the box before submitting the form.', 'yith-datatables-hg' );
				$color   = 'red';

			} else {

				if ( isset( $_POST['raffle_data'] ) ) {

					if ( '' === $user_data['user_name'] || '' === $user_data['user_surname'] || ! filter_var( $user_data['user_email'], FILTER_VALIDATE_EMAIL ) ) { 

						$mensaje    = esc_html__( 'An error has occured, please, fill in the fields .' , 'yith-datatables-hg' );
						$color      = 'red';
						$valid_data = false;
					} else {
						$name   = $user_data['user_name'];
						$l_name = $user_data['user_surname'];
						$email  = $user_data['user_email'];
					}
				} else {
					$user   = wp_get_current_user();
					$name   = $user->user_nicename;
					$l_name = $user->display_name;
					$email  = $user->user_email;
				}

				if ( $valid_data ) {
					global $wpdb;
					$user_regist = $wpdb->get_results( 'SELECT email FROM wp_yith_raffle' );
					$registered  = false;
					$index       = 0;
					$max         = count( $user_regist );

					while ( ! $registered && $index < $max ) {
						if ( $email === $user_regist[ $index ]->email ) {
							$registered = true;
						}
						++$index;
					}

					if ( ! $registered ) {

						$table_name = $wpdb->prefix . 'yith_raffle';
						$wpdb->insert(
							$table_name,
							array(
								'name'    => $name,
								'surname' => $l_name,
								'email'   => $email,
							)
						);
						$mensaje = esc_html__( 'Congratulations, you are in the raffle Good luck!', 'yith-datatables-hg' );
						$color   = 'green';
					} else {
						$mensaje = esc_html__( 'This user has already signed up.', 'yith-datatables-hg' );
						$color   = 'red';
					}
				}
			}
				wp_send_json(
					array(
						'message'   => $mensaje,
						'color_msg' => $color,
					)
				);
		}
	}
}
