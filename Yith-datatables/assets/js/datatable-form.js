jQuery( function( $ ) {
    
    $("#raffle-submit").on('click', function() {
    
      var raffle_data ={ 
       user_name: $('#raffle-name').val(),
       user_surname: $('#raffle-surname').val(),
       user_email: $('#raffle-email').val(),
        }
        require = ($('#raffle-require').prop('checked')) ? 'yes' : 'no';
       $.ajax({
          type: "POST",
          dataType: "json",
          url: 'wp-admin/admin-ajax.php',
          data: {action: 'take_raffle', raffle_data:raffle_data , require:require },
          
          error: function (response){
              console.log(response);
          },
          success: function(response) {
            $('#raffle-msg').text(response['message']);
            $('#raffle-msg').css('color',response['color_msg']);
        }
            
        });
    });
});