<?php //phpcs:ignore

/**
 * Plugin Name: YITH Datatables HG
 * Description: YITH Datatables plugin
 * Version: 1.0.0
 * Author: Héctor García
 * Author URI: https://yithemes.com/
 * Text Domain: yith-datatables-hg
 */

! defined( 'ABSPATH' ) && exit;   // Before all, check if defined ABSPATH.

/*Create some constant where defined PATH for Style, Assets, Templates, Views */

if ( ! defined( 'YITH_DTH_VERSION' ) ) {
	define( 'YITH_DTH_VERSION', '1.0.0' );
}

if ( ! defined( 'YITH_DTH_DIR_URL' ) ) {
	define( 'YITH_DTH_DIR_URL', plugin_dir_url( __FILE__ ) );
}

if ( ! defined( 'YITH_DTH_DIR_ASSETS_URL' ) ) {
	define( 'YITH_DTH_DIR_ASSETS_URL', YITH_DTH_DIR_URL . 'assets' );
}

if ( ! defined( 'YITH_DTH_DIR_ASSETS_CSS_URL' ) ) {
	define( 'YITH_DTH_DIR_ASSETS_CSS_URL', YITH_DTH_DIR_ASSETS_URL . '/css' );
}

if ( ! defined( 'YITH_DTH_DIR_ASSETS_JS_URL' ) ) {
	define( 'YITH_DTH_DIR_ASSETS_JS_URL', YITH_DTH_DIR_ASSETS_URL . '/js' );
}

if ( ! defined( 'YITH_DTH_DIR_PATH' ) ) {
	define( 'YITH_DTH_DIR_PATH', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'YITH_DTH_DIR_INCLUDES_PATH' ) ) {
	define( 'YITH_DTH_DIR_INCLUDES_PATH', YITH_DTH_DIR_PATH . 'includes' );
}

if ( ! defined( 'YITH_DTH_DIR_TEMPLATES_PATH' ) ) {
	define( 'YITH_DTH_DIR_TEMPLATES_PATH', YITH_DTH_DIR_PATH . '/templates' );
}

if ( ! defined( 'YITH_DTH_DIR_VIEWS_PATH' ) ) {
	define( 'YITH_DTH_DIR_VIEWS_PATH', YITH_DTH_DIR_PATH . 'views' );
}

// Different way to declare a constant.

! defined( 'YITH_DTH_INIT' )               && define( 'YITH_DTH_INIT', plugin_basename( __FILE__ ) );
! defined( 'YITH_DTH_SLUG' )               && define( 'YITH_DTH_SLUG', 'yith-datatables-hg' );
! defined( 'YITH_DTH_SECRETKEY' )          && define( 'YITH_DTH_SECRETKEY', 'zd9egFgFdF1D8Azh2ifA' );
! defined( 'YITH_DTH_OPTIONS_PATH' )       && define( 'YITH_DTH_OPTIONS_PATH', YITH_DTH_DIR_PATH . 'plugin-options' );

/**
 * Include the scripts
 */
if ( ! function_exists( 'yith_dth_init_classes' ) ) {

	/**
	 * Yith_dth_init_classes
	 *
	 * @return void
	 */
	function yith_dth_init_classes() {

		load_plugin_textdomain( 'yith-datatables-hg', false, basename( dirname( __FILE__ ) ) . '/languages' );

		// Require all the files you include on your plugins. Example.
		require_once YITH_DTH_DIR_INCLUDES_PATH . '/class-yith-datatables-hg.php';

		if ( class_exists( 'YITH_DTH_Datatables_Hg' ) ) {
			/*
			*	Call the main function
			*/
			yith_dth_datatables_hg();
		}
	}
}


add_action( 'plugins_loaded', 'yith_dth_init_classes', 11 );


if ( ! function_exists( 'yith_dth_install' ) ) {
	/**
	 * Yith_dth_install.
	 *
	 * @return void
	 */
	function yith_dth_install() {

			do_action( 'yith_dth_init' );
			YITH_DTH_DB::install();

	}

	add_action( 'plugins_loaded', 'yith_dth_install', 11 );
}
