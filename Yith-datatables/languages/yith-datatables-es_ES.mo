��          �      �       H  2   I  1   |     �     �  
   �     �     �      �     �          )  2   B     u  $  �  >   �  ;   �     +     2  
   B     M     U  #   ]     �     �     �  ;   �     
           	          
                                            An error has occured, please, fill in the fields . Congratulations, you are in the raffle Good luck! Email: Héctor García Last name: Name: Raffle! This user has already signed up. YITH Datatables HG YITH Datatables plugin Yes! i wanna participate You must check the box before submitting the form. https://yithemes.com/ Project-Id-Version: YITH Datatables HG 1.0.0
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/Yith-datatables
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2021-04-05 14:37+0100
X-Generator: Poedit 2.4.2
X-Domain: yith-datatables-hg
Last-Translator: 
Plural-Forms: nplurals=2; plural=(n != 1);
Language: es_ES
X-Poedit-KeywordsList: __;_e;_n;_x;_ex;_nx;esc_attr__;esc_attr_e;esc_attr_x;esc_html__;esc_html_e;esc_html_x;_n_noop;_nx_noop;translate_nooped_plural
 Ha ocurrido algún error, por favor, rellene todos los campos. Felicidades, está participando en la rifa, ¡Buena suerte! Email: Héctor García Apellidos: Nombre: ¡Rifa! Este usuario ya ha sido registrado. YITH Tablas de datos HG YITH Tablas de datos plugin Si, ¡quiero participar! Tienes que marcar la casilla antes de enviar el formulario. https://yithemes.com/ 