<?php
/**
 * This file belongs to the YITH DTH Datatables.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

?> <div class='raffle'> 
<?php
if ( ! is_user_logged_in() ) {
	?>
				<ul>
					<li>
						<label for="name"><?php _e( 'Name:', 'yith-datatables-hg' ) ?></label>
						<input type="text" name="raffle-name" id="raffle-name" />
					</li>
<br>
<hr>
<br>
					<li>
						<label for="surname"><?php _e( 'Last name:', 'yith-datatables-hg' ) ?></label>
						<input type="text" name="raffle-surname" id="raffle-surname" />
<br>
<hr>
<br>
					<li>
						<label for="email"><?php _e( 'Email:', 'yith-datatables-hg' ) ?> </label>
						<input type="text" name="raffle-email" id="raffle-email"/>
					</li>
<br>
<hr>
<?php } ?>
               <li>
               <input type='checkbox' name='raffle-require' id='raffle-require'/>
               <label for='require'><?php _e('Yes! i wanna participate', 'yith-datatables-hg' ) ?></label> 
               </li>
<br>
					<li>
						<button name="raffle-submit" id="raffle-submit"><?php _e( 'Raffle!', 'yith-datatables-hg' ) ?> </button>
					</li>
<br>
				</ul>
				<p name='raffle-msg' id='raffle-msg'> </p>
</div>
