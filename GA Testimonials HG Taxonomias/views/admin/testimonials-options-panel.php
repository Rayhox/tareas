<?php 

?>

<div class="wrap">
    <h1><?php esc_html_e('Testimonials options', 'yith-testimonials-hg' );?></h1>

    <form method="post" action="options.php">
        <?php
            settings_fields( 'tth-options-page' );
		    do_settings_sections( 'tth-options-page' );
           esc_html__( submit_button('Modify') );
        ?>

    </form>
</div>

