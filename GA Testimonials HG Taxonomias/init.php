<?php

/*
 * Plugin Name: YITH Testimonials HG
 * Description: YITH Testimonials plugin
 * Version: 1.0.0
 * Author: Héctor García 
 * Author URI: https://yithemes.com/
 * Text Domain: yith-testimonials-hg
 */

! defined( 'ABSPATH' ) && exit;   //Before all, check if defined ABSPATH.

/*Create some constant where defined PATH for Style, Assets, Templates, Views */

if ( ! defined( 'YITH_TTH_VERSION' ) ) {
	define( 'YITH_TTH_VERSION', '1.0.0' );
}

if ( ! defined( 'YITH_TTH_DIR_URL' ) ) {
	define( 'YITH_TTH_DIR_URL', plugin_dir_url( __FILE__ ) );
}

if ( ! defined( 'YITH_TTH_DIR_ASSETS_URL' ) ) {
	define( 'YITH_TTH_DIR_ASSETS_URL', YITH_TTH_DIR_URL . 'assets' );
}

if ( ! defined( 'YITH_TTH_DIR_ASSETS_CSS_URL' ) ) {
	define( 'YITH_TTH_DIR_ASSETS_CSS_URL', YITH_TTH_DIR_ASSETS_URL . '/css' );
}

if ( ! defined( 'YITH_TTH_DIR_ASSETS_JS_URL' ) ) {
	define( 'YITH_TTH_DIR_ASSETS_JS_URL', YITH_TTH_DIR_ASSETS_URL . '/js' );
}

if ( ! defined( 'YITH_TTH_DIR_PATH' ) ) {
	define( 'YITH_TTH_DIR_PATH', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'YITH_TTH_DIR_INCLUDES_PATH' ) ) {
	define( 'YITH_TTH_DIR_INCLUDES_PATH', YITH_TTH_DIR_PATH . 'includes' );
}

if ( ! defined( 'YITH_TTH_DIR_TEMPLATES_PATH' ) ) {
	define( 'YITH_TTH_DIR_TEMPLATES_PATH', YITH_TTH_DIR_PATH . '/templates' );
}

if ( ! defined( 'YITH_TTH_DIR_VIEWS_PATH' ) ) {
	define( 'YITH_TTH_DIR_VIEWS_PATH', YITH_TTH_DIR_PATH . 'views' );
}

// Different way to declare a constant

! defined( 'YITH_TTH_INIT' )               && define( 'YITH_TTH_INIT', plugin_basename( __FILE__ ) );
! defined( 'YITH_TTH_SLUG' )               && define( 'YITH_TTH_SLUG', 'yith-testimonials-hg' );
! defined( 'YITH_TTH_SECRETKEY' )          && define( 'YITH_TTH_SECRETKEY', 'zd9egFgFdF1D8Azh2ifA' );
! defined( 'YITH_TTH_OPTIONS_PATH' )       && define( 'YITH_TTH_OPTIONS_PATH', YITH_TTH_DIR_PATH . 'plugin-options' );

/**
 * Include the scripts
 */
if ( ! function_exists( 'yith_tth_init_classes' ) ) {

	function yith_tth_init_classes() {

		load_plugin_textdomain( 'yith-testimonials-hg', false, basename( dirname( __FILE__ ) ) . '/languages' );

		//Require all the files you include on your plugins. Example
		require_once YITH_TTH_DIR_INCLUDES_PATH . '/class-yith-testimonials-hg.php';

		if ( class_exists( 'YITH_TTH_Testimonials_Hg' ) ) {
			/*
			*	Call the main function
			*/
			yith_tth_testimonials_hg();
		}
	}
}


add_action( 'plugins_loaded', 'yith_tth_init_classes', 11 );