<?php
/*
 * This file belongs to the YITH TTH Testimonials.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
   //error_log(print_r($hover_effect, true));
?>

<!-- CONTAINER GENERAL, VIP Y EFECTO -->
<div  <?php
    switch($hover_effect){
         
      case 'zoom' : 
                     if(get_post_meta( $post->ID, 'info_tth_vip', true) === 'on'){?>
                     class='yith-tth-zoom-vip'
                     <?php ;}else {?>
                        class = 'yith-tth-zoom'
                     <?php ;}?>
      <?php     break;
      case 'highlight':
                     if(get_post_meta( $post->ID, 'info_tth_vip', true) === 'on'){?>
                     class='yith-tth-highlight-vip'
                     <?php ;}else {?>
                        class = 'yith-tth-highlight'
                     <?php ;}?>
                     
      <?php     break;
      default :
                     if(get_post_meta( $post->ID, 'info_tth_vip', true) === 'on'){?>
                     class='yith-tth-container-vip'
                     <?php ;}else {?>
                     class = 'yith-tth-container'
                     <?php ;}?> 
      <?php     break; }?>>                
            
                  <!-- CABECERA GENERAL-->
         <div class="yith-tth-container-header">
               <?php if ( 'yes' === $show_image ) { ?>
                  <!-- IMAGEN -->
               <div class="yith-tth-image">
               <?php echo get_the_post_thumbnail( $post->ID, 'thumbnail', array( 'class' => 'yith-tth-image-img' ) ); ?>
               </div>
                <?php }; ?>
          <!-- FIN IMAGEN -->
                  <!-- TITULO CABECERA -->
            <div class="yith-tth-head">
               
            <!-- TITULO -->
            <h4 class="yith-tth-title"><?php echo esc_html( $post->post_title ); ?></h4>
               <!-- FIN TITULO -->
                
               <!-- ROL -->
               <h5 class="yith-tth-subtitle"><?php echo esc_html( get_post_meta( $post->ID, 'info_tth_role', true ) ); ?> 
                <!-- FIN ROL -->
                
                <!-- COMPAÑIA + ENLACE -->
               at <a href= " <?php get_post_meta( $post->ID, 'info_tth_company_url', true); ?> ">
                
                <?php echo esc_html( get_post_meta( $post->ID, 'info_tth_company', true ) ); ?></a></h5>
                <!-- FIN COMPAÑIA + ENLACE -->
                <!-- EMAIL -->
               <div>
                  <h5 class="yith-tth-subtitle"><?php echo esc_html( get_post_meta( $post->ID, 'info_tth_email', true ) ); ?>
               <!-- FIN EMAIL -->
               </div>
                <!-- RATING -->
                <div class="yith-tth-rating">
                  <p>
                     <?php 
                     
                     switch(get_post_meta( $post->ID, 'info_tth_rating', true )){
//Amarilla : &#11088
//Negra : &#9733
                        case '1' :
                           echo '&#11088 &#9733; &#9733; &#9733; &#9733;';
                           break;
                        case '2' :
                           echo '&#11088; &#11088; &#9733; &#9733; &#9733;';
                           break;
                        case '3' :
                           echo '&#11088; &#11088; &#11088; &#9733; &#9733;';
                            break; 
                        case '4' :
                           echo '&#11088; &#11088; &#11088; &#11088; &#9733;';
                           break;
                        case '5' :
                           echo '&#11088; &#11088; &#11088; &#11088; &#11088;';
                           break;
                           
                        default :
                           echo  '&#11088 &#9733; &#9733; &#9733; &#9733;';
                              break;
                        }
                     
                     
                     ?>
                  </p>
                  <!-- FIN RATING -->
               </div> 
            <!-- FIN TITULO CABECERA -->
            </div>
                  <!-- BADGE    -->
          <?php if(get_post_meta( $post->ID, 'info_tth_badge', true)==='on'){?>
               <div class='yith-tth-badge'>
                     <span class="yith-tth-badge"><?php echo esc_html( get_post_meta( $post->ID, 'info_tth_badge_text', true ) ); ?></span>
          <!-- FIN BADGE -->
                  </div>
                  <?php }?>
         <!-- FIN CABECERA    -->
          </div>
      <!-- CONTENIDO BOX -->
      <div class="yith-tth-container-content">
         <!-- CONTENIDO -->
         <div class="yith-tth-body">
            <?php echo $post->post_content;?>
            <!-- FIN CONTENIDO -->
         </div>
         <!-- FIN CONTENIDO BOX -->
        </div>
      <!-- FIN GENERAL EFECTO Y VIP -->
     </div>