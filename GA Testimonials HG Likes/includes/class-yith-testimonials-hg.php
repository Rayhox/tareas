<?php
/**
 * This file belongs to the YITH Testimonials HG.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'YITH_TTH_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_TTH_Testimonials_Hg' ) ) {
	/**
	 * YITH_TTH_Testimonials_Hg
	 */
	class YITH_TTH_Testimonials_Hg {

		/**
		 * Main Instance
		 *
		 * @var YITH_TTH_Testimonials_Hg
		 * @since 1.0
		 * @access private
		 */

		private static $instance;
		/**
		 * Main Admin Instance
		 *
		 * @var YITH_TTH_Testimonials_Hg_Admin
		 * @since 1.0
		 */
		public $admin = null;
		/**
		 * Main Frontpage Instance
		 *
		 * @var YITH_TTH_Testimonials_Hg_Frontend
		 * @since 1.0
		 */
		public $frontend = null;

		/**
		 * Main Shortcodes Instance
		 *
		 * @var YITH_TTH_Testimonials_Hg_Frontend
		 * @since 1.0
		 */
		public $shortcodes = null;

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_TTH_Testimonials_Hg Main instance
		 * @author Héctor García <hectorgarciam95@gmail.com>
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();

		}
		/**
		 * YITH_TTH_Testimonials_Hg constructor.
		 */
		private function __construct() {
			$require = apply_filters(
				'yith_tth_require_class',
				array(
					'common'   => array(
						'includes/class-yith-tth-post-types.php',
						'includes/functions.php',
						'includes/class-yith-tth-shortcodes.php',
						'includes/class-yith-tth-ajax.php',

					),
					'admin'    => array(
						'includes/class-yith-tth-admin.php',
					),
					'frontend' => array(
						'includes/class-yith-tth-frontend.php',
					),
				)
			);

			$this->_require( $require );

			$this->init_classes();

			$this->init();
		}
		/**
		 * Add the main classes file
		 *
		 * Include the admin and frontend classes
		 *
		 * $main_classes array The require classes file path
		 *
		 * @author Héctor García <hectorgarciam95@gmail.com>
		 * @since  1.0
		 *
		 * @return void
		 * @access protected
		 */
		protected function _require( $main_classes ) {
			foreach ( $main_classes as $section => $classes ) {
				foreach ( $classes as $class ) {
					if ( 'common' == $section || ( 'frontend' == $section && !is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) || ( 'admin' == $section && is_admin() ) && file_exists( YITH_TTH_DIR_PATH . $class ) ) {
						require_once( YITH_TTH_DIR_PATH . $class );
					}
				}
			}
		}

		/**
		 * Init common class if they are necessary
		 * @author Héctor García <hectorgarciam95@gmail.com>
		 * @since  1.0
		 **/
		public function init_classes() {
			$this->shortcodes = YITH_TTH_Shortcodes::get_instance();
			YITH_TTH_Post_Types::get_instance();
			YITH_TTH_AJAX::get_instance();
		}

		/**
		 * Function init()
		 *
		 * Instance the admin or frontend classes
		 *
		 * @author Héctor García <hectorgarciam95@gmail.com>
		 * @since  1.0
		 * @return void
		 * @access protected
		 */
		public function init() {
			if ( is_admin() ) {
				$this->admin = YITH_TTH_Admin::get_instance();
			}

			if ( ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
				$this->frontend = YITH_TTH_Frontend::get_instance();
			}
		}

	}
}
/**
 * Get the YITH_TTH_Testimonials_Hg instance
 *
 * @return YITH_TTH_Testimonials_Hg
 */
if ( ! function_exists( 'yith_tth_testimonials_hg' ) ) {
	/**
	 * Yith_tth_testimonials_hg
	 *
	 * @return void
	 */
	function yith_tth_testimonials_hg() {
		return YITH_TTH_Testimonials_Hg::get_instance();
	}
}
