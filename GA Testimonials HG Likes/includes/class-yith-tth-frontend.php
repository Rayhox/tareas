<?php //phpcs:ignore
/**
 * This file belongs to the YITH Testimonials HG.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_TTH_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_TTH_Frontend' ) ) {

	/**
	 * YITH_TTH_Frontend
	 */
	class YITH_TTH_Frontend {

		/**
		 * Main Instance
		 *
		 * @var YITH_TTH_Frontend
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_TTH_Frontend Main instance
		 * @author Héctor García <hectorgarciam95@gmail.com>
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_PS_Frotend constructor.
		 */
		private function __construct() {

			add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );

		}
		/**
		 * Enqueue_scripts
		 *
		 * @return void
		 */
		public function enqueue_scripts() {
			wp_register_style( 'yith-tth-shortcode-css', YITH_TTH_DIR_ASSETS_CSS_URL . '/yith-tth-shortcode.css', array(), YITH_TTH_VERSION );
			wp_register_script( 'main-js', YITH_TTH_DIR_ASSETS_JS_URL . '/main.js', array( 'jquery' ), YITH_TTH_VERSION );
			wp_enqueue_script( 'main-js' );

		}


	}
}
