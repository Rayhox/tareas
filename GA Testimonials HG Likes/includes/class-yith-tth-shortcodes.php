<?php //phpcs:ignore
/**
 * This file belongs to the YITH Testimonials HG.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_TTH_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_TTH_Shortcodes' ) ) {
	/**
	 * YITH_TTH_Shortcodes
	 */
	class YITH_TTH_Shortcodes {

		/**
		 * Main Instance
		 *
		 * @var YITH_TTH_Shortcodes
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_TTH_Post_Types Main instance
		 * @author Héctor García <hectorgarciam95@gmail.com>
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}
		/**
		 * YITH_TTH_Shortcode constructor.
		 */
		private function __construct() {

			$shortcodes = array(
				'yith_testimonials' => __CLASS__ . '::yith_display_testimonials',
			);
			foreach ( $shortcodes as $shortcode => $function ) {
				add_shortcode( $shortcode, $function );
			}
		}
		/** // phpcs:ignore
		 * Custom shortcode to display testimonials and make transient for them.
		 *
		 * $atts
		 * @return void 
		 */
		public static function yith_display_testimonials( $atts ) {

			wp_enqueue_style( 'yith-tth-shortcode-css' );

			$custom_options_borderradius = get_option( 'yith_tth_shortcode_borderradius', '7' );
			$custom_options_color        = get_option( 'yith_tth_shortcode_color', '#0073aa' );
			$custom_options_css          =
				"
					.yith-tth-testimonials {
							border-radius:  {$custom_options_borderradius}px;
						}
					.yith-tth-testimonials a{
							color: {$custom_options_color};
						}
				";

			// Encolando las opciones custom al css ya existente.
			wp_add_inline_style( 'yith-tth-shortcode-css', $custom_options_css );

				$custom_tax = isset( $atts['estudiante'] ) && ! empty( $atts['estudiante'] ) ? $atts['estudiante'] : false;

				$show_image = get_option( 'yith_tth_shortcode_show_image', 'yes' );

				$hover_effect = get_option( 'yith_tth_shortcode_effect', 'zoom' );

				$id_list = isset( $atts['ids'] ) ? explode( ',', $atts['ids'] ) : false;

				$number = isset( $atts['number'] ) ? $atts['number'] : get_option( 'yith_tth_shortcode_number', 6 );

			$args = array(
				'numberposts'  => $number,
				'post_type'    => 'yith_hg_testimonial',
				'include'      => $id_list,
				'show_image'   => $show_image,
				'hover_effect' => $hover_effect,
				'tax_query'    => '',
			);

			if ( $custom_tax ) {
				$args['tax_query'] = array(
					'taxonomy' => 'yith_tth_estudiante_tax',
					'field'    => 'slug',
					'terms'    => $custom_tax,
				);
			}

			// Set up the transient.
			$posts_list = get_transient( 'yith_tth_custom_transient' );

			if ( ! $posts_list ) {

				//error_log( print_r( 'Entro en el if de ! $posts_list', true )); //phpcs:ignore

				$posts_list = get_posts( $args );
				set_transient( 'yith_tth_custom_transient', $posts_list, 2 * HOUR_IN_SECONDS );

			}
				ob_start();

				// Bucle para los testimonios cargando la vista.
			foreach ( $posts_list as $post ) {
				yith_tth_get_template(
					'/frontend/show_testimonials.php',
					array(
						'post'	 	   => $post,
						'show_image'   => $args['show_image'],
						'hover_effect' => $hover_effect,
					)
				);
			}
				//error_log( print_r( $posts_list, true));
			return '<div class = "yith-tth-testimonials" >' . ob_get_clean() . '</div>';
		}
	}
}
