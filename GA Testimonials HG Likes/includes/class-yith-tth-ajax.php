<?php //phpcs:ignore
/**
 * This file belongs to the YITH Testimonials HG.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_TTH_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_TTH_AJAX' ) ) {

	/**
	 * YITH_TTH_AJAX
	 */
	class YITH_TTH_AJAX {

		/**
		 * Main Instance
		 *
		 * @var YITH_TTH_AJAX
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_TTH_AJAX Main instance
		 * @author Héctor García <hectorgarciam95@gmail.com>
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_TTH_AJAX constructor.
		 */
		private function __construct() {
			add_action( 'wp_ajax_update_likes', array( $this, 'update_likes' ) );
		}
		/**
		 * Updating likes through AJAX
		 *
		 * @return void
		 */
		public function update_likes() {
			if ( isset( $_POST['post_id'] ) && isset( $_POST['event'] ) ) {
				$post_id = $_POST['post_id'];
				$event   = $_POST['event'];
				$likes   = get_post_meta( $post_id, 'info_tth_likes', true );
				$updated = false;
				$user_id = get_current_user_id();

				if ( 'up' == $event ) {
					update_post_meta(
						$post_id,
						'info_tth_likes',
						$likes += 1
					);
					$updated = true;
					update_user_meta(
						$user_id,
						$post_id,
						'yes',
					);
				} elseif ( 'down' == $event ) {
					update_post_meta(
						$post_id,
						'info_tth_likes',
						$likes -= 1
					);
					$updated = true;
					update_user_meta(
						$user_id,
						$post_id,
						'yes',
					);
				}

				if ( $updated ) {
					wp_send_json( array( 'message' => $likes . ' likes' ) );
				} else {
					wp_send_json( array( 'message' => __( 'Message not received', 'yith-testimonials-hg' ) ) );
				}
			}

		}

	}
}
