jQuery( function( $ ) {
    
    $(".yith-tth-likes-up, .yith-tth-likes-down").on('click', function() {
      var post_data ={ 
       post_id: $( this ).parent().attr("data-id"),
       event: $( this ).attr("data-event"),
       action: 'update_likes',
        }
      var likes = $( this ).parent().find(">:first-child"); 
      var arrows = $( this ).parent().find('a'); 
 
       $.ajax({
          type: "POST",
          dataType: "json",
          url: 'wp-admin/admin-ajax.php',
          data: post_data,
          error: function (response){
              console.log(response);
          },
          success: function(response) {
                likes.html( response['message'] );
                $(arrows).hide('slow');
            }
            
        });
    });
});