<?php

?>

<div>
    <p>
		<label>Role</label>
		<input type="text" name="info_tth_role" value="<?php echo esc_html( $role ); ?>" />
	</p>
	<p>
		<label>Company</label>
		<input type="text"  name="info_tth_company" value="<?php echo esc_html( $company ); ?>" />
	</p> 
    <p>
		<label>Company URL</label>
		<input type="text"  name="info_tth_company_url" value="<?php echo esc_html( $company_url ); ?>" />
	</p>
    <p>
		<label>Email</label>
		<input type="email"  name="info_tth_email" value="<?php echo esc_html( $tth_email ); ?>" />
	</p>
	<p>  
		<label>Rating</label>  
        <select name="info_tth_rating">  
            <option <?php selected( $rating, '1' ); ?>>1</option>  
            <option <?php selected( $rating, '2' ); ?>>2</option>  
            <option <?php selected( $rating, '3' ); ?>>3</option> 
            <option <?php selected( $rating, '4' ); ?>>4</option> 
            <option <?php selected( $rating, '5' ); ?>>5</option> 
        </select>  
	</p>
    <p>
                <label>VIP?</label>
                <input type="checkbox" name="info_tth_vip" value="on" <?php checked( get_post_meta( $post->ID, 'info_tth_vip', true ), 'on', true ); ?>><label>Yes</label>
                
            
                <input type="checkbox" name="info_tth_vip" value="off" <?php checked( get_post_meta( $post->ID, 'info_tth_vip', true ), 'off', true ); ?>><label>No</label>
    </p>
    <p>
                <label>Enable Badge</label>
                <input type="checkbox" name="info_tth_badge" value="on" <?php checked( get_post_meta( $post->ID, 'info_tth_badge', true ), 'on', true ); ?>><label>Yes</label>

                <input type="checkbox" name="info_tth_badge" value="off" <?php checked( get_post_meta( $post->ID, 'info_tth_badge', true ), 'off', true ); ?>><label>No</label>

    </p>
	<p>
		<label>Badge Text</label>
		<input type="text" name="info_tth_badge_text" value="<?php echo esc_html( $badge_text ); ?>" />
	</p>
</div>