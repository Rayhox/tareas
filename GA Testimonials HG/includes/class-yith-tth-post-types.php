<?php
/*
 * This file belongs to the YITH Testimonials HG.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_TTH_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_TTH_Post_Types' ) ) {

	class YITH_TTH_Post_Types {

        /**
		 * Main Instance
		 *
		 * @var YITH_TTH_Post_Types
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		/**
		 * Post type name
		 *
		 * @var YITH_TTH_Post_Types
		 * @since 1.0
		 * @access public
		 */
		public static $post_type = 'yith_hg_testimonial';

		
        /**
         * Main plugin Instance
         *
         * @return YITH_TTH_Post_Types Main instance
         * @author Héctor García <hectorgarciam95@gmail.com>
         */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
        }
        
		/**
		 * YITH_TTH_Post_Types constructor.
		 */
		private function __construct() {
			add_action( 'init', array( $this, 'setup_testimonial_post_type' ) );

		}

		/**
		 * Setup the 'Testimonials' custom post type
		 */
		public function setup_testimonial_post_type() {
            
			$labels = array(
                'name'    => _x( 'CPT Testimonials', 'Post type author name', 'yith-testimonials-hg' ),
                'message' => _x( 'yith_hg_testimonial', 'Post type message', 'yith-testimonials-hg' ),
                'photo'   => _x( 'yith_hg_testimonial', 'Post type photo', 'yith-testimonials-hg' ),
            );
			
			$args = array(
				'labels'        => $labels,
				'description'  =>  __('Testimonial post type','yith-testimonials-hg'),
				'public'       => false,
				'menu_icon'    => 'dashicons-universal-access',
				'show_in_menu' => true,
				'show_ui'      => true,
				'rewrite'      => false,
				'supports'     => array( 'title', 'editor', 'author', 'thumbnail' ),
			);
			register_post_type( self::$post_type, $args );
		}

	}	
}