<?php
/*
 * This file belongs to the YITH Testimonials HG.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_TTH_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_TTH_Shortcodes' ) ) {

	class YITH_TTH_Shortcodes {

        /**
		 * Main Instance
		 *
		 * @var YITH_TTH_Shortcodes
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		
        /**
         * Main plugin Instance
         *
         * @return YITH_TTH_Post_Types Main instance
         * @author Héctor García <hectorgarciam95@gmail.com>
         */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
        }
        
		/**
		 * YITH_TTH_Shortcode constructor.
		 */
		private function __construct() {
            
			$shortcodes = array(
				'yith_testimonials'       => __CLASS__ . '::yith_display_testimonials',
			);
			foreach ( $shortcodes as $shortcode => $function ) {
				add_shortcode( $shortcode, $function );
			}
		}
		
		
		public static function yith_display_testimonials(){
			
			wp_enqueue_style('yith-tth-shortcode-css');

			$custom_options_borderradius = get_option( 'yith_tth_shortcode_borderradius', '7' );
			$custom_options_color        = get_option( 'yith_tth_shortcode_color', '#0073aa' );
			$custom_options_css          = 
				"
					.yith-tth-testimonials {
							border-radius:  {$custom_options_borderradius}px;
						}
					.yith-tth-testimonials a{
							color: {$custom_options_color};
						}
				";
			
			//Encolando las opciones custom al css ya existente
			wp_add_inline_style( 'yith-tth-shortcode-css', $custom_options_css );
			
			
			$show_image = get_option('yith_tth_shortcode_show_image', 'yes');
			$hover_effect = get_option('yith_tth_shortcode_effect', 'zoom');
			$id_list = isset( $atts['ids'] )  ?  explode(',', $atts['ids']) : false;
			$number = isset( $atts['number' ])  ?  $atts['number'] : get_option( 'yith_tth_shortcode_number', 6 ) ;
			
			
			$args = array(
                 'numberposts' => $number,
                'post_type'   => 'yith_hg_testimonial',
                'include '    => $id_list,
				'show_image'  => $show_image,
				'hover_effect'=> $hover_effect,
            );

			$posts_list     = get_posts( $args );
		
			//error_log(print_r( $hover_effect ,true));

			ob_start();
			
			//Bucle para los testimonios cargando la vista
			foreach ( $posts_list as $post ) {
				yith_tth_get_template( '/frontend/show_testimonials.php', array(
					'post' 			  => $post,
					'show_image'      => $args['show_image'],
					'hover_effect'    => $hover_effect,
				) );
			}
			//error_log(print_r($class_effect,true));
			
			return'<div class="yith-tth-testimonials">' . ob_get_clean() . '</div>';
    	}


	}	
}